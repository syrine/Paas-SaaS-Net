﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using System;
using WebApplication.Models;

namespace WebApplication
{
    public class Program
    {


        public static void Main(string[] args)
        {



            var config = new ConfigurationBuilder()
                    .AddCommandLine(args).AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .Build();

            var host = new WebHostBuilder()
                        .UseKestrel()
                        .UseConfiguration(config)
                        .UseStartup<Startup>()
                        .Build();

           //   string connectionString = config.GetConnectionString("SampleConnection");
           string connectionString = config.GetConnectionString("DefaultConnection");
            
           //   Create an employee instance and save the entity to the database
           var entry = new User() { email = "syriiiiine@esprit.tn" };

            var entry2 = new User()
            {
                email = "safwene.test@esprit.tn"
            };
        


            using (var context = HunterViewContextFactory.Create(connectionString))
            {
                //    context.Database.EnsureCreated();
                //    context.user.Add(new JobSeeker { name ="jjj", domain = "Donald" , years=2015});
                //    context.user.Add(
                //        new HeadHunter
                //        {city ="cieiii"
                //        });



                //  context.Add(entry);
                // context.SaveChanges();


                context.Add(entry2);
                context.SaveChanges();


            }

            Console.WriteLine($"Employee was saved in the database with id: {entry.email}");


            host.Run();

        }


    }
}
